
chrome.commands.onCommand.addListener((command) => {
  if (command === 'close-tab') {
    chrome.tabs.query({currentWindow: true, active: true}, ([tab]) => {
      chrome.tabs.remove(tab.id)
    })
  }
})
